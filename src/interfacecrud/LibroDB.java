/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacecrud;

import java.util.List;

/**
 *
 * @author ignacio
 */
public interface LibroDB {
    
    // Return an ArrayList
    public List<Libro> consultarLibros();
    
    public Long registrarLibro(Libro libro);
    
    public void eliminarLibro(Long id);
    
    public void actualizarLibro(Libro libro);
    
    public Libro consultarLibro(Long id);
}
